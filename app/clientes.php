<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    protected $primaryKey = 'id';

    protected $fillable=['id','nombre','appaterno','apmaterno','rfc','plataforma','usuario','contrasena','direccion'];
    protected $date=['deleted_at'];
}
