<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
 
 //unauthenticated guests
 public function __construct()

 {
    $this->middleware('guest' , ['only' => 'showLoginForm']);
 }
 

 public function showLoginForm()

 {
    return view('auth.login');
 }

//credentiles for log in
 public function login()
 {
    $credentiales = $this->validate(request(),[
//validate
        'email' => 'email|required|string',
        'password' => 'required|string'
    ]);


    if (Auth::attempt($credentiales))
    {
        return redirect()->route('altaclient');
    }


    return back()->withErrors(['email' => trans('auth.failed')])
    ->withInput(request(['email']));

 }

 public function logout()

 {
    Auth::logout();

    return redirect('/login');
 }

}
