<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

use App\clientes;
use Laracasts\Flash\Flash;

class clientesc extends Controller
{
//customers view
    public function altaclientes()
    {
        return view('clientes.clientes');
    }

// messages flash construct

    public function __construct()
    {
        $this->middleware('auth');
    }


// function for save data
    public function guardaclientes(Request $request)
    {
  //declaration of form variables
                 $nombre=$request->nombre;
                 $appaterno=$request->appaterno;
                 $apmaterno=$request->apmaterno;
                 $rfc=$request->rfc;
                 $plataforma=$request->plataforma;
                 $usuario=$request->usuario;
                 $contrasena=$request->contrasena;
                 $direccion=$request->direccion;
////validate of  form///

                   $this->validate($request,
                    [
                    'nombre'=>'required|regex:/^[A-Z][A-Z a-z]{3,100}$/',
                    'rfc'=>['required:/^[A-Z]{3,4}[0-9]{6}[A-Z,0-9]{2,3}$/'],
                    'plataforma'=>'required',
                    'usuario'=>'required',
                    'contrasena'=>'required',
                    'direccion'=>'required',
                ]);

// new objet
                 $cl = new clientes;
                 $cl->nombre=$request->nombre;
                 $cl->appaterno=$request->appaterno;
                 $cl->apmaterno=$request->apmaterno;
                 $cl->rfc=$request->rfc;
                 $cl->plataforma=$request->plataforma;
                 $cl->usuario=$request->usuario;
                 $cl->contrasena=$request->contrasena;
                 $cl->direccion=$request->direccion;


                 //save array data
                 $cl->
//encrypted data
/*                 fill([

                    'usuario' => encrypt($request->usuario),
                    'contrasena' => encrypt($request->contrasena)
        ])-> */
                 save();

                flash('Empresa Registrada de forma exitosa!');
                return redirect()->route('altaclient');

    }
//show the data	
	
	public function registros()
	{
		$mc =clientes::orderBy('id')->paginate('3');
		return view ('clientes.muestraclientes')
		->with('mc',$mc);
	}
//delete data for id	
	public function deletecliente($id)
	{
	clientes::find($id)->delete();
	//Message flash delete successful
                    flash('Empresa    '. ''  .$id. '' .'    Eliminada de forma exitosa !');
                return redirect()->route('muestraregistros');
	}
//function query id to update
	public function editaclientes($id)
	{
		$clientes=clientes::where('id',$id)->get();
		return view('clientes.modificaclientes')
		->with('clientes',$clientes[0]);

	}
	
	
	public function editaclientes2(Request $request)
	{

        
//declaration of form variables for update

		         $id=$request->id;
		         $nombre=$request->nombre;
                 $appaterno=$request->appaterno;
                 $apmaterno=$request->apmaterno;
                 $rfc=$request->rfc;
                 $plataforma=$request->plataforma;
                 $usuario=$request->usuario;
                 $contrasena=$request->contrasena;
                 $direccion=$request->direccion;
				 
////validate of  form///

                   $this->validate($request,
                    [
                    'nombre'=>'required|regex:/^[A-Z][A-Z a-z]{3,100}$/',
                    'appaterno'=>'regex:/^[A-Z][A-Z a-z]{3,60}$/',
                    'apmaterno'=>'regex:/^[A-Z][A-Z a-z]{3,60}$/',
                    'rfc'=>['required:/^[A-Z]{3,4}[0-9]{6}[A-Z,0-9]{2,3}$/'],
                    'plataforma'=>'required',
                    'usuario'=>'required',
                    'contrasena'=>'required',
                    'direccion'=>'required',
                ]);				 
				 	
//new data for update and save                    		 
				 $cli=clientes::find($id);
				 $cli->id=$request->id;
				 $cli->nombre=$request->nombre;
                 $cli->appaterno=$request->appaterno;
                 $cli->apmaterno=$request->apmaterno;
                 $cli->rfc=$request->rfc;
                 $cli->plataforma=$request->plataforma;
                 $cli->usuario=$request->usuario;
                 $cli->contrasena=$request->contrasena;
                 $cli->direccion=$request->direccion;
				 
//method save successful				 
                 $cli->save();

//Message flash update successful
        flash('Empresa con clave'. '  '.$id. ' '.
            'y nombre'.' '.$nombre.'  '. 'ha sido Modificada de forma exitosa!');
//redirect to table customers
                        return redirect()->route('muestraregistros');			 


	}
}
