
@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-4 col-md-offset-4">

			<div class="panel panel-default">
				<div class="panel-heading">
					
				
				<h1 class="panel-title"> Acesso a la aplicación</h1>
			    </div>

			    <div class="inner-addon left-addon">
			    	<form method="post" action="{{route('login')}}">
			    		{{ csrf_field() }}

			    		<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}" 
			    			 class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-user"></i>

			    			<label for='email'>Email</label>
			    			
			    			<input class="form-control" 
			    			type="email"  
			    			name="email" 
			    			value="{{ old('email') }}" 
			    			placeholder="Ingresa tu correo">

			    			{!! $errors->first('email','<span class="help-block">:message</span>') !!}
			    			
			    		</div>

				    		<div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
			    			<label for='password'>Contraseña</label>
			    			<input class="form-control" 
			    			type="password"  
			    			name="password" 
			    			placeholder="Ingresa tu Contraseña">
			    			{!! $errors->first('password','<span class="help-block">:message</span>') !!}

			    			
			    		</div>
			    		   <br><br>
    <input type='submit' value='Acesso' class="submit" />
    			    	</form>
<a href="index.html/../.."><img src="arrow.png" style="width:100px; height:100px;"></a>
			    </div>
			    </div>
			</div>
			
		</div>
	</div>
@endsection