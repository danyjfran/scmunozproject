
@extends('index2')


@section('cliente')
<center><H3>MODIFICACIÓN DEL CLIENTES <H3/></center>

<center>

<form  action ="{{route('editacli2')}}"  method="POST">
   
   {{ csrf_field() }}

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
			     ID<input type="text" class="form-control" name="id"  placeholder="ID"required readonly='readonly' value="{{$clientes->id}}">
    </div> 
 
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
			     Nombre<input type="text" class="form-control" name="nombre"  placeholder="Nombre"required value="{{$clientes->nombre}}">
    @if($errors->first('nombre'))
    <i>{{$errors->first('nombre')}}</i>@endif
    </div>
	
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
            <label for="am">
	            Apellido Paterno<input type="text" class="form-control" name="appaterno" size="40" placeholder="Apellido Paterno" value="{{$clientes->appaterno}}">
        @if($errors->first('appaterno'))
    <i>{{$errors->first('appaterno')}}</i>@endif
    </div>	



	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
	            Apellido Materno<input type="text" class="form-control" name="apmaterno" size="40"  placeholder="Apellido Materno" value="{{$clientes->apmaterno}}">
    @if($errors->first('apmaterno'))
    <i>{{$errors->first('apmaterno')}}</i>@endif  
	</div>

	<div class="inner-addon left-addon">
	
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                RFC<input type="text" class="form-control" name="rfc"   placeholder="RFC" required value="{{$clientes->rfc}}">
    @if($errors->first('rfc'))
    <i>{{$errors->first('rfc')}}</i>@endif  
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Plataforma<input type="text" class="form-control" name="plataforma" size="40" placeholder="Plataforma" required value="{{$clientes->plataforma}}">
    @if($errors->first('plataforma'))
    <i>{{$errors->first('plataforma')}}</i>@endif
	</div>
	
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Usuario<input type="text" class="form-control" name="usuario"   placeholder="Usuario" required value="{{$clientes->usuario}}">
    @if($errors->first('usuario'))
    <i>{{$errors->first('usuario')}}</i>@endif
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Contraseña<input type="text" class="form-control" name="contrasena"  placeholder="Contraseña" required value="{{$clientes->contrasena}}">
    @if($errors->first('contrasena'))
    <i>{{$errors->first('contrasena')}}</i>@endif
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Dirección<input type="text" class="form-control" name="direccion" size="40"  placeholder="Dirección web" required value="{{$clientes->direccion}}">
    @if($errors->first('direccion'))
    <i>{{$errors->first('direccion')}}</i>@endif
	</div>

    <br><br>
    <input type='submit' value='MODIFICAR' class="submit" />
    <input type='reset' value='LIMPIAR' class="submit" />

</form>

</center>

@stop
