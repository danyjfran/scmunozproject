
@extends('index')


@section('contenido')
    <meta charset="utf-8">

<center><H3>REGISTRAR UN CLIENTES <H3/></center>
<center>
<form  action ="{{route('guardacliente')}}"  method="POST">

    {{ csrf_field() }}
    
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-user"></i>
             <label for="am">
			     Nombre<input type="text" class="form-control" name="nombre"  placeholder="Nombre" value="{{old('nombre')}}">
    @if($errors->first('nombre'))
    <i>{{$errors->first('nombre')}}</i>@endif
    </div>
	
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-user"></i>	
            <label for="am">
	            Apellido Paterno<input type="text" class="form-control" name="appaterno" size="40" placeholder="Apellido Paterno" value="{{old('appaterno')}}">
        @if($errors->first('appaterno'))
    <i>{{$errors->first('appaterno')}}</i>@endif
    </div>	


	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-user"></i>	
             <label for="am">
	            Apellido Materno<input type="text" class="form-control" name="apmaterno" size="40"  placeholder="Apellido Materno" value="{{old('apmaterno')}}">
    @if($errors->first('apmaterno'))
    <i>{{$errors->first('apmaterno')}}</i>@endif               
	</div>

	<div class="inner-addon left-addon">
	
        <i class="glyphicon glyphicon-adjust"></i>	
             <label for="am">
                RFC<input type="text" class="form-control" name="rfc"   placeholder="RFC" value="{{old('rfc')}}">
    @if($errors->first('rfc'))
    <i>{{$errors->first('rfc')}}</i>@endif                
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Plataforma<input type="text" class="form-control" name="plataforma" size="40" placeholder="Plataforma" value="{{old('plataforma')}}">
    @if($errors->first('plataforma'))
    <i>{{$errors->first('plataforma')}}</i>@endif
	</div>
	
	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-log-in"></i>	
             <label for="am">
                Usuario<input type="text" class="form-control" name="usuario"   placeholder="Usuario" value="{{old('usuario')}}">
    @if($errors->first('usuario'))
    <i>{{$errors->first('usuario')}}</i>@endif
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-lock"></i>	
             <label for="am">
                Contraseña<input type="password" class="form-control" name="contrasena"  placeholder="Contraseña" value="{{old('contrasena')}}">
    @if($errors->first('contrasena'))
    <i>{{$errors->first('contrasena')}}</i>@endif
	</div>

	<div class="inner-addon left-addon">
        <i class="glyphicon glyphicon-cloud"></i>	
             <label for="am">
                Dirección<input type="text" class="form-control" name="direccion" size="40"  placeholder="Dirección web" value="{{old('direccion')}}">
    @if($errors->first('direccion'))
    <i>{{$errors->first('direccion')}}</i>@endif
	</div>

    <br><br>
    <input type='submit' value='REGISTRO' class="submit" />
    <input type='reset' value='LIMPIAR' class="submit" />

</form>

</center>

@stop
