@extends('index')

@section('cliente')  
  <meta charset="utf-8">


		<!-- TABLAS RESPONSIVE -->
       <center> <H3><p class="enunciado_tabla">CARTERA DE CLIENTES</p><H3/> <center/>
        <div class="table-responsive">
         <table class="table table-hover">
           	<!-- tbody es el cuerpo de la tabla -->
           	<tbody>
           		<tr class="warning">
				
				<td>Clave</td><td>Nombre</td><td>Apellido Paterno</Td><td>Apellido Materno</td>
		        <td>RFC</td><td>Plataforma</td><td>Usuario</td><td>Contrasena</td><td>Direccion</td>
		         <td colspan='2'>Acciones</td></tr>
		 
		 @foreach($mc as $m)
         <tr><td>{{$m->id}}</td><td>{{$m->nombre}}</td>
         <td>{{$m->appaterno}}</td><td>{{$m->apmaterno}}</td>
		 <td>{{$m->rfc}}</td><td>{{$m->plataforma}}</td>
		 <td>{{$m->usuario}}</td><td>{{$m->contrasena}}</td>
		 <td>{{$m->direccion}}</td>
		 <td><a href="{{URL::action('clientesc@deletecliente',['id'=>$m->id])}}">
		 <img src="delete.png" height="20" width="20"></a></td>
		 
		 <td><a href="{{URL::action('clientesc@editaclientes',['id'=>$m->id])}}">
		 <img src="update.png" height="20" width="20"></a></td>

	 
		 </tr>
         @endforeach
           	</thead>
         </table>
		 		{{$mc->render()}}		

        </div>
@stop
