-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2019 a las 06:15:18
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `clientes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appaterno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apmaterno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plataforma` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `appaterno`, `apmaterno`, `rfc`, `plataforma`, `usuario`, `contrasena`, `direccion`, `remember_token`, `created_at`, `updated_at`) VALUES
(13, 'Mayra', 'Gonazales', 'Parra', 'pagm235689kdi', 'linkin', 'danus', '1234567890', 'www.link.com.mx', NULL, '2019-04-02 01:58:53', '2019-04-06 00:29:58'),
(14, 'Mari', 'Gonazales', 'Guerrero', 'GOPC238929rj1', 'Facebook', 'dany_922e', '123456789', 'www.twwiter.com.mx', NULL, '2019-04-02 02:03:34', '2019-04-02 02:03:34'),
(18, 'Alejandra', 'Vega', 'Guerrero', 'VEGA923839JI2', 'Facebook', 'ale.vega', 'damaris', 'facebook.com', NULL, '2019-04-03 22:35:06', '2019-04-03 22:40:35'),
(19, 'Aurelio', 'Cruz', 'Mondagron', 'GOPC238929rj1', 'Twwiter', 'aure92_32', '123456789', 'www.twwiter.com.mx', NULL, '2019-04-05 23:59:04', '2019-04-05 23:59:04'),
(20, 'Daniela', 'Adamari', 'Apolo', 'GOPC238929rj1', 'Instagram', 'Cielito', '1234567890', 'www.link.com.mx', NULL, '2019-04-06 00:03:50', '2019-04-06 00:03:50'),
(21, 'Alejandra', 'Gonazales', 'Apolo', 'JUFD96041RJ1', 'Instagram', 'ale.gonza', 'asdfghj', 'www.instagram.com.mx', NULL, '2019-04-06 00:06:07', '2019-04-06 00:06:46'),
(23, 'Kimber', 'Martinez', 'Apolo', 'JUFD96041RJ1', 'Facebook', 'Cielito', '1234567', 'www.twwiter.com.mx', NULL, '2019-04-06 00:13:29', '2019-04-06 00:13:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2019_02_25_183359_create_clientes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kayden Kuhlman I', 'nickolas.streich@example.net', '2019-04-24 21:19:29', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'hYnTahtngBKaVJvclmds8XpRDbGcXj4yJPFYE6Mz37uXPZi9RISp2ppocmSg', '2019-03-23 04:55:04', '2019-03-23 04:55:04'),
(2, 'Myrtis Zemlak', 'pkulas@example.org', '2019-03-26 04:54:20', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'G9XUbCvA4s', '2019-03-26 04:54:20', '2019-03-26 04:54:20'),
(3, 'Brook Dickinson', 'treutel.reva@example.org', '2019-03-26 04:54:25', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'TRBSJsGUGS', '2019-03-26 04:54:25', '2019-03-26 04:54:25'),
(4, 'Karley Schultz', 'thomas87@example.com', '2019-03-26 04:54:27', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'XgZlsyPFkc', '2019-03-26 04:54:27', '2019-03-26 04:54:27'),
(5, 'Jesse Lueilwitz', 'hardy44@example.org', '2019-03-26 04:54:27', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'KgbPDaEJYF', '2019-03-26 04:54:27', '2019-03-26 04:54:27'),
(6, 'Dr. Brendan Legros IV', 'grice@example.net', '2019-03-26 04:54:29', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Njx9xCDLlE', '2019-03-26 04:54:29', '2019-03-26 04:54:29'),
(7, 'Dr. Adonis Wilkinson Sr.', 'wyman24@example.net', '2019-03-26 04:54:30', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '74pS1KQPJe', '2019-03-26 04:54:30', '2019-03-26 04:54:30'),
(8, 'Corene Smitham MD', 'jamie.little@example.net', '2019-04-01 23:26:59', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'JUFzTUmZGMBnxX1bPTLCMwhVAHBfcTmBU7zzv6kQGwEhc4pCnrCdAeWhYFrw', '2019-03-26 04:54:31', '2019-03-26 04:54:31'),
(9, 'Merl Kertzmann Sr.', 'paolo16@example.net', '2019-04-12 17:29:07', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'HzN3p36bv67ov8dxpBv5hOCRAjrsV57bEGQI5kUoc3KTOD2mllS2zNczWE3w', '2019-03-26 04:54:32', '2019-03-26 04:54:32'),
(10, 'Dr. Katelyn Cronin', 'schoen.lonzo@example.com', '2019-03-26 04:54:33', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'aSVrZo4r6p', '2019-03-26 04:54:33', '2019-03-26 04:54:33'),
(11, 'Marian Hintz', 'shanelle.ullrich@example.org', '2019-04-23 02:07:55', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'WLQc6yGEcm', '2019-04-23 02:07:55', '2019-04-23 02:07:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
