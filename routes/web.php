<?php
use App\clientes;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//crud of customer for register, delete or alter
Route::get('/altaclientes','clientesc@altaclientes')->name('altaclient');
Route::POST('/guardacliente','clientesc@guardaclientes')->name('guardacliente');
Route::get('/muestraclientes','clientesc@registros') ->name ('muestraregistros');
Route::get('/borrac/{id}','clientesc@deletecliente')->name('deletec');
Route::get('/editaclientes/{id}','clientesc@editaclientes')->name('editacli');
Route::POST('/editacli/','clientesc@editaclientes2')->name('editacli2');


/*
Route::get('/login', function () {
    return view('auth.login');
});
*/

//route of login
Route::get('/login','Auth\LoginController@showLoginForm');


Route::POST('login','Auth\LoginController@login')->name('login');
Route::POST('logout','Auth\LoginController@logout')->name('logout');